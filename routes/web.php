<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'EmployeeController@index');

// create new employee
Route::get('employee/add', 'EmployeeController@add');
Route::post('employee/store', 'EmployeeController@store');

// edit employee
Route::get('employee/{id}/edit', 'EmployeeController@edit');
Route::put('employee/update/{id}', 'EmployeeController@update');
Route::get('employee/change_status/{id}', 'EmployeeController@change_status');
