<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' => 'required|max:255',
            'firstname' => 'required|max:255',
            'middlename' => 'required|max:255',
            'nickname' => 'required|max:255',
            'department' => 'required|max:255',
            'position' => 'required|max:255',
            'birthdate' => 'required',
            'hireddate' => 'required',
            'email' => 'required|email|max:255',
            'status' => 'required',
        ];
    }
}
