<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Employeedetail;
use App\Http\Requests\EmployeeFormRequest;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the employees.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $employees = Employee::all();
        
        return view('employees.viewall', compact('employees'));
    }

    /**
     * Show the form for add new employee.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('employees.create_employee');
    }

    /**
     * Store a new employee.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeFormRequest $request)
    {
        $emp_data = new Employee();
        $emp_data->lastname = $request->get('lastname');
        $emp_data->firstname = $request->get('firstname');
        $emp_data->middlename = $request->get('middlename');
        $emp_data->nickname = $request->get('nickname');
        $emp_data->birthdate = $request->get('birthdate');
        $emp_data->email = $request->get('email');
        $emp_data->save();
        
        $emp_details = new Employeedetail();
        $emp_details->employee_id = $emp_data->id;
        $emp_details->dept = $request->get('department');
        $emp_details->position = $request->get('position');
        $emp_details->hireddate = $request->get('hireddate');
        $emp_details->status = $request->get('status');
        $emp_details->save();
    
        return redirect('/')->with('success', 'Employee data is successfully saved');
    }

    /**
     * Show the form for editing the employee.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::findOrFail($id);

        return view('employees.edit_employee', compact('employee'));
    }

    /**
     * Update the employee details.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeFormRequest $request, $id)
    {
        $emp_data = Employee::find($id);
        $emp_data->lastname = $request->get('lastname');
        $emp_data->firstname = $request->get('firstname');
        $emp_data->middlename = $request->get('middlename');
        $emp_data->nickname = $request->get('nickname');
        $emp_data->birthdate = $request->get('birthdate');
        $emp_data->email = $request->get('email');
        $emp_data->save();

        $emp_details = Employeedetail::where('employee_id',$id);
        $emp_details->dept = $request->get('department');
        $emp_details->position = $request->get('position');
        $emp_details->hireddate = $request->get('hireddate');
        $emp_details->status = $request->get('status');
        
        return redirect('/')->with('success', 'Employee data is successfully saved');
    }

    /**
     * Update the status of the employee.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_status($id)
    {
        $status = Employeedetail::where('employee_id',$id)->first()->status;
        Employeedetail::where('employee_id',$id)->update(['status' => ($status == 1 ? 0 : 1)]);
        
        return redirect('/')->with('success', 'Employee successfully change status');
    }

    
}
