<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{	
    public function empdetails()
    {
    	return $this->hasOne(Employeedetail::class);
    }
}
