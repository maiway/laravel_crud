<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('employees')->delete();
    	DB::table('employeedetails')->delete();

    	$records = array(
    		array(
    			'lastname' 	=> 'Flamel',
            	'firstname' => 'Nicolas',
            	'middlename'=> 'Pontoise',
            	'nickname' 	=> 'Nic',
            	'birthdate' => '1985-01-01',
            	'email' 	=> 'flamelnic@sample.com',
            	'dept' 		=> 'HR',
            	'position'	=> 'Manager',
            	'hireddate' => '2020-01-01',
            	'status' 	=> 1
    		),
    		array(
    			'lastname' 	=> 'Potter',
            	'firstname' => 'Helen',
            	'middlename'=> 'Beatrix',
            	'nickname' 	=> 'len',
            	'birthdate' => '1986-02-01',
            	'email' 	=> 'potterhellen@sample.com',
            	'dept' 		=> 'Finance',
            	'position'	=> 'Payroll Assistant',
            	'hireddate' => '2020-02-01',
            	'status' 	=> 1
    		),
    		array(
    			'lastname' 	=> 'Malaya',
            	'firstname' => 'Noah',
            	'middlename'=> 'Poblacion',
            	'nickname' 	=> 'Jose',
            	'birthdate' => '1987-03-01',
            	'email' 	=> 'poblacionjose@sample.com',
            	'dept' 		=> 'Marketing',
            	'position'	=> 'Manager',
            	'hireddate' => '2020-03-01',
            	'status' 	=> 0
    		),
    		array(
    			'lastname' 	=> 'Hellis',
            	'firstname' => 'William',
            	'middlename'=> 'Sprite',
            	'nickname' 	=> 'Wil',
            	'birthdate' => '1988-04-01',
            	'email' 	=> 'helliswil@sample.com',
            	'dept' 		=> 'IT',
            	'position'	=> 'Project Manager',
            	'hireddate' => '2020-04-01',
            	'status' 	=> 1
    		),
    		array(
    			'lastname' 	=> 'Millais',
            	'firstname' => 'John',
            	'middlename'=> 'Everette',
            	'nickname' 	=> 'John',
            	'birthdate' => '1989-05-01',
            	'email' 	=> 'millaisjohn@sample.com',
            	'dept' 		=> 'Records',
            	'position'	=> 'Custodian',
            	'hireddate' => '2020-05-01',
            	'status' 	=> 1
    		),

    	);

    	foreach ($records as $key => $record) {

    		DB::table('employees')->insert([
    		    'lastname' 	=> $record['lastname'],
    		    'firstname' => $record['firstname'],
    		    'middlename'=> $record['middlename'],
    		    'nickname' 	=> $record['nickname'],
    		    'birthdate' => $record['birthdate'],
    		    'email' 	=> $record['email'],
                'created_at'=> new DateTime
    		]);
    		DB::table('employeedetails')->insert([
    		    'employee_id'=> $key+1,
    		    'dept' 		=> $record['dept'],
    		    'position'	=> $record['position'],
    		    'hireddate' => $record['hireddate'],
    		    'status' 	=> $record['status'],
                'created_at'=> new DateTime
    		]);
    	}

    }
}
