@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  <h1>View All Employees</h1>
  <a class="btn btn-primary" href="{{ url('employee/add') }}">Add New Employee</a>
  <br><br>
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Fullname</th>
        <th>Department</th>
        <th>Position</th>
        <th>Age</th>
        <th>Hire Date</th>
        <th>Email Address</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($employees as $employee)
        <tr>
          <td>{{ $employee->lastname.', '.$employee->firstname.' '.$employee->middlename[0].'.' }}</td>
          <td>{{ $employee->empdetails->dept }}</td>
          <td>{{ $employee->empdetails->position }}</td>
          <td>{{ Carbon\Carbon::parse($employee->birthdate)->age }}</td>
          <td>{{ Carbon\Carbon::parse($employee->empdetails->hireddate)->format('M. d, Y') }}</td>
          <td>{{ $employee->email }}</td>
          <td>{{ $employee->empdetails->status == 1 ? 'Active' : 'Resigned' }}</td>
          <td>
            <a href="{{ url('employee/'.$employee->id.'/edit') }}" class="btn btn-success">Edit</a>
            <a href="{{ url('employee/change_status/'.$employee->id) }}" class="btn btn-dark">Change Status</a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
<div>
@endsection