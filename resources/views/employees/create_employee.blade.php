@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    <h4>Add New Employee</h4>
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif
      <form method="post" action="store">
          <div class="form-group">
              @csrf
              <label for="lastname">Last Name:</label>
              <input type="text" class="form-control @error('lastname') is-invalid @enderror"
                name="lastname" value="{{ old('lastname') }}"/>
          </div>
          <div class="form-group">
              <label for="firstname">First Name:</label>
              <input type="text" class="form-control @error('firstname') is-invalid @enderror"
                name="firstname" value="{{ old('firstname') }}"/>
          </div>
          <div class="form-group">
              <label for="middlename">Middle Name:</label>
              <input type="text" class="form-control @error('middlename') is-invalid @enderror"
                name="middlename" value="{{ old('middlename') }}"/>
          </div>
          <div class="form-group">
              <label for="nickname">Nick Name:</label>
              <input type="text" class="form-control @error('nickname') is-invalid @enderror"
                name="nickname" value="{{ old('nickname') }}"/>
          </div>
          <div class="form-group">
              <label for="department">Department:</label>
              <input type="text" class="form-control @error('department') is-invalid @enderror"
                name="department" value="{{ old('department') }}"/>
          </div>
          <div class="form-group">
              <label for="position">Position:</label>
              <input type="text" class="form-control @error('position') is-invalid @enderror"
                name="position" value="{{ old('position') }}"/>
          </div>
          <div class="form-group">
              <label for="birthdate">Birth Date:</label>
              <input type="date" class="form-control @error('birthdate') is-invalid @enderror"
                name="birthdate" value="{{ old('birthdate') }}"/>
          </div>
          <div class="form-group">
              <label for="hireddate">Hired Date:</label>
              <input type="date" class="form-control @error('hireddate') is-invalid @enderror"
                name="hireddate" value="{{ old('hireddate') }}"/>
          </div>
          <div class="form-group">
              <label for="email">Email Address:</label>
              <input type="text" class="form-control @error('email') is-invalid @enderror"
                name="email" value="{{ old('email') }}"/>
          </div>
          <div class="form-group">
              <label for="status">Status:</label>
              <select class="form-control @error('status') is-invalid @enderror"
                name="status"/>
                <option></option>
                <option value="1" {{ old('status') == 1 ? 'selected' : '' }}>Active</option>
                <option value="0" {{ old('status') != '' && old('status') == 0 ? 'selected' : '' }}>Resigned</option>
              </select>
          </div>

          <button type="submit" class="btn btn-primary">Submit Employee Data</button>
          <a class="btn btn-dark" href="{{ url('/') }}">Cancel</a>
      </form>
  </div>
</div>
@endsection