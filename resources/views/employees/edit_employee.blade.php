@extends('layout')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    <h4>Edit Employee</h4>
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif
      <form method="post" action="{{ url('employee/update/'.$employee->id) }}">
          <div class="form-group">
              @csrf
              @method('PUT')
              <label for="lastname">Last Name:</label>
              <input type="text" class="form-control @error('lastname') is-invalid @enderror"
                name="lastname" value="{{ $employee->lastname }}"/>
          </div>
          <div class="form-group">
              <label for="firstname">First Name:</label>
              <input type="text" class="form-control @error('firstname') is-invalid @enderror"
                name="firstname" value="{{ $employee->firstname }}"/>
          </div>
          <div class="form-group">
              <label for="middlename">Middle Name:</label>
              <input type="text" class="form-control @error('middlename') is-invalid @enderror"
                name="middlename" value="{{ $employee->middlename }}"/>
          </div>
          <div class="form-group">
              <label for="nickname">Nick Name:</label>
              <input type="text" class="form-control @error('nickname') is-invalid @enderror"
                name="nickname" value="{{ $employee->nickname }}"/>
          </div>
          <div class="form-group">
              <label for="department">Department:</label>
              <input type="text" class="form-control @error('department') is-invalid @enderror"
                name="department" value="{{ $employee->empdetails->dept }}"/>
          </div>
          <div class="form-group">
              <label for="position">Position:</label>
              <input type="text" class="form-control @error('position') is-invalid @enderror"
                name="position" value="{{ $employee->empdetails->position }}"/>
          </div>
          <div class="form-group">
              <label for="birthdate">Birth Date:</label>
              <input type="date" class="form-control @error('birthdate') is-invalid @enderror"
                name="birthdate" value="{{ $employee->birthdate }}"/>
          </div>
          <div class="form-group">
              <label for="hireddate">Hired Date:</label>
              <input type="date" class="form-control @error('hireddate') is-invalid @enderror"
                name="hireddate" value="{{ $employee->empdetails->hireddate }}"/>
          </div>
          <div class="form-group">
              <label for="email">Email Address:</label>
              <input type="text" class="form-control @error('email') is-invalid @enderror"
                name="email" value="{{ $employee->email }}"/>
          </div>
          <div class="form-group">
              <label for="status">Status:</label>
              <select class="form-control @error('status') is-invalid @enderror"
                name="status"/>
                <option value="1" {{ $employee->status == 1 ? 'selected' : '' }}>Active<option>
                <option value="0" {{ $employee->status != '' && $employee->status == 0 ? 'selected' : '' }}>Resigned<ption>
              </select>
          </div>

          <button type="submit" class="btn btn-primary">Submit Employee Data</button>
          <a class="btn btn-dark" href="{{ url('/') }}">Cancel</a>
      </form>
  </div>
</div>
@endsection