@extends('layout')

@section('content')
	<h1>{{ $employee->lastname.', '.$employee->firstname.' '.$employee->middlename[0].'.' }}</h1>

	<div>
		Department: {{ $employee->empdetails->dept }}<br>
		Position: {{ $employee->empdetails->position }}<br>
		Age: {{ Carbon\Carbon::parse($employee->birthdate)->age }}<br>
		Hire Date: {{ $employee->empdetails->hireddate }}<br>
		Email Address: {{ $employee->email }}<br>
		Status: {{ $employee->empdetails->status == 1 ? 'Active' : 'Resigned' }}
	</div>
@stop